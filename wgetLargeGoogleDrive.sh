#	Source https://medium.com/@acpanjan/download-google-drive-files-using-wget-3c2c025a8b99
#	Files should be set as anyone with the link can see
#	Use this for "large" files, files > 100MB, for "small" files, files < 100MB, use wgetSmallGoogleDrive.sh
#	$1 is fileID, e.g., in the following url fileID is "1ReO16YNDOTwpgMdQYklCKXtiZNeA705G" url = https://drive.google.com/file/d/1ReO16YNDOTwpgMdQYklCKXtiZNeA705G/view?usp=sharing
#	$2 is fileName where to save
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id='$1 -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=$1" -O $2 && rm -rf /tmp/cookies.txt
