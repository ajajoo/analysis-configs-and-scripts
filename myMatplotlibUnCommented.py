from __future__ import division
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as mpatches
import matplotlib.ticker as ticker 
import numpy as np
from numpy import cumsum
import os
import csv
from itertools import izip
import re
from collections import Counter
import heapq
import math
from termcolor import colored
import random
import time
import sys
import plotly
import plotly.graph_objects as go
#import plotly.express as px

def plotlyVectors(graphType, multi = False, barAlign = "center" ,x=[], y=[], logx=False, logy=False, logBase=10, xlabel="", ylabel="", fileName="AJgraph", title = "", color=[], xTickLabels = [], yTickLabels = [], xLim=[], yLim=[], xMinorTickLabels = [], yMinorTickLabels = [], xMinorTickLabelsLocator = [], yMinorTickLabelsLocator = [], hGrid = False, vGrid = True, png = False, pdf = False, eps = False, html = False, figHeight = 0, figWidth = 0, xlabelFontSize = 20, ylabelFontSize = 20, xTickLabelFontSize = 18, yTickLabelFontSize = 18, lineWidth = 1, lineType = [], lineColors = [], legends=[], legendLoc = 1, legendFontSize = 16, show = False, returnObject = False, annotateText = [], rotateXTicks = 0, rotateYTicks = 0, scatterSize = [], mergeBottom = False, bottomLimit = None, bottomMergedValue = None, mergeTop = False, topLimit = None, topMergedValue = None, markers = [], plot3D = False, z = [], zlabel = ""):
        if multi:
            if graphType != "cdf" and graphType != "scatter" and graphType != "":
		alertMessage("plotlyVectors API can only plot multiple Scatter plot for now.")
		return
	    if len(legends) != len(x):
                alertMessage("plotlyVectors: Incase of multi you need to pass exactly 1 legend value corresponding to each x vectoy. Here number of legends = "+str(len(lengends)+" and number of x vectors = "+str((len(x)))))
		return
	fig = go.Figure()
	if graphType == "scatter":
	    if multi:	# In this case it assumes and x, y and legends are lists of lists and have values corresponding to each other a same index.
                scatter_marker = []
                if len(markers) == 0:
                    scatter_marker = ['o' for i in range(len(x))]
                else:
                    scatter_marker = markers
                for i in range(len(x)): #   for color you need to have a list which has element corresponding to each scatter POINT. However, for marker only one entry is needed for a sactter plot.
                    #print "In plotlyVectors loop: ", i, " legends[i]: ", legends[i]
                    #print y[i]
                    if len(scatterSize) != 0:
                        #temp_markers = ['o', '^', 'x', '+', 'd', "1", "2", "3", "4", "8", "|"]
                        fig.add_trace(go.Scatter(x=x[i], y=y[i], name=legends[i]))
		    else:
                        fig.add_trace(go.Scatter(x=x[i], y=y[i], name=legends[i]))
		legendsFromPlot = True
	    else:
                if len(scatterSize) != len(x):
                    #fig = px.scatter(x = x, y = y)
                    fig.add_trace(go.Scatter(x=x, y=y))
                else:
                    #fig = px.scatter(x = x, y = y)
                    fig.add_trace(go.Scatter(x=x, y=y))
			#plt.show()
        fig.update_layout(
            xaxis_title=xlabel,
            yaxis_title=ylabel,
            font=dict(
                family="Courier New, monospace",
                size=min(xlabelFontSize, ylabelFontSize),
                color="#7f7f7f"
            )
        )
        print "going to save file at ", fileName+'.html'
        if html:
            plotly.offline.plot(fig, filename = fileName+'.html', auto_open = False)
        if pdf:
            fig.write_image(fileName+'.pdf')
        if eps:
            fig.write_image(fileName+'.eps')

def plotVectors(graphType, multi = False, barAlign = "center" ,x=[], y=[], logx=False, logy=False, logBase=10, xlabel="", ylabel="", fileName="AJgraph", title = "", titleFontSize = 20, color=[], xTicks = [], yTicks = [], xTickLabels = [], yTickLabels = [], xLim=[], yLim=[], xMinorTickLabels = [], yMinorTickLabels = [], xMinorTickLabelsLocator = [], yMinorTickLabelsLocator = [], hGrid = False, vGrid = True, png = False, pdf = False, eps = False, figHeight = 0, figWidth = 0, tight = True, xlabelFontSize = 20, ylabelFontSize = 20, xTickLabelFontSize = 18, yTickLabelFontSize = 18, lineWidth = 1, lineType = [], lineColors = [], legends=[], legendLoc = 1, legendFontSize = 16, show = False, returnObject = False, annotateText = [], rotateXTicks = 0, rotateYTicks = 0, scatterSize = [], mergeBottom = False, bottomLimit = None, bottomMergedValue = None, mergeTop = False, topLimit = None, topMergedValue = None, markers = [], plot3D = False, z = [], zlabel = "", subPlots = False, printPlots = False):
# plots a graph of graphType. All the parameter names are self explanatory
# if want to plot cdf of anything pass it as x and pass y as empty vector or any thing, it wont effect but do not skip it
        #temp_colors = ['blue', 'red', 'green', 'black', 'orange', "brown", "limegreen", "yellow", "m", "c"]
        matplotlib.rcParams['pdf.fonttype'] = 42
        matplotlib.rcParams['ps.fonttype'] = 42
        matplotlib.rcParams['axes.unicode_minus'] = False
        legendsFromPlot = False
        if multi:
            if graphType != "cdf" and graphType != "scatter" and graphType != "":
                alertMessage("API can only plot multiple CDFs or Scatter or normal x-y line plot for now.")
                return
            if len(legends) != len(x):
                print alertMessage("Incase of multi you need to pass legends.")
                return
        fig, ax = matplotlib.pyplot.subplots()
        if plot3D:
           if z == []:
	       alertMessage("z entries are needed to plot 3d figure.")
	       return
           import matplotlib.pyplot as plt
           from mpl_toolkits.mplot3d import Axes3D
           fig = plt.figure()
           ax = fig.gca(projection='3d')
	if figHeight != 0:
		fig.set_figheight(figHeight)
	if figWidth != 0:
		fig.set_figwidth(figWidth)
	if len(color) == 0:
		color = ['b' for i in range(len(x))]
	graphType.lower()
	if graphType == "scatter":
                if logx:
                    ax.set_xscale('log')
                if logy:
                    ax.set_yscale('log')
		if multi:	# In this case it assumes and x, y and legends are lists of lists and have values corresponding to each other a same index.
                        scatter_marker = []
                        if len(markers) == 0:
                            scatter_marker = ['o' for i in range(len(x))]
                        else:
                            scatter_marker = markers

                        for i in range(len(x)): #   for color you need to have a list which has element corresponding to each scatter POINT. However, for marker only one entry is needed for a sactter plot.
                            if len(scatterSize) != 0:
                                #temp_markers = ['o', '^', 'x', '+', 'd', "1", "2", "3", "4", "8", "|"]
                                ax.scatter(x[i], y[i], label=legends[i], color = color[i], s=scatterSize[i], marker=scatter_marker[i])
			    else:
                                ax.scatter(x[i], y[i], label=legends[i], color = color[i], marker=scatter_marker[i])
			legendsFromPlot = True
		else:
                    if len(scatterSize) != len(x):
			ax.scatter(x,y,color=color, facecolors='none')
                    else:
			ax.scatter(x,y,color=color,s=scatterSize, facecolors='none', linewidth=lineWidth)

                    #ax.plot([i for i in range(int(max(x)))], [1 for i in range(int(max(x)))], color = 'black')
                    #ax.plot([150 for i in range(int(max(y)))], [i for i in range(int(max(y)))], color = 'black')
		    #plt.show()
                if annotateText !=[]:
                    for i, val in enumerate(annotateText):
                        ax.annotate(val, (x[i], y[i]))
	elif graphType == "bar":
		ax.bar(x,y,align=barAlign)
	elif graphType == "":
            if multi:
                lType = ''
                if subPlots:
                    import matplotlib.pyplot as plt
                    fig, ax = plt.subplots(len(x),1,sharex=True)
                    #import matplotlib as mpl
                    #mpl.rcParams['axes.linewidth'] = 0.05 #set the value globally 
                    for j, subx in enumerate(x):
		        if lineType != []:
		            lType = lineType[j]
                        ax[j].plot(subx, y[j], lType, label=legends[j], color=color[j], linewidth = lineWidth)
                        _min = min(y[j])
                        _max = max(y[j])
                        k = 5
                        if _max >= 100:
                            k = 10
                        if _max >= 1000:
                            k = 100
                        y_ticks = [roundNToNearestMultipleOfK(_min,k), roundNToNearestMultipleOfK(_min*0.66 + _max*0.33,k), roundNToNearestMultipleOfK(_min*0.33 + _max*0.66,k), roundNToNearestMultipleOfK(_max,k)]
                        if _max*_min <0:
                            y_ticks = [0, roundNToNearestMultipleOfK(_max*0.33,k), roundNToNearestMultipleOfK(_max*0.66,k), roundNToNearestMultipleOfK(_max,k)]
                        if j == 3:
                           y_ticks = [0,50,90,140]
                        if j == 4:
                           y_ticks = [0,3,7,10]#[0,300,700,1000]#
                        if j == 5:
                           y_ticks = [0,3,7,10]#[0,300,700,1000]#
                        ax[j].set_yticks(y_ticks)
                        plt.setp(ax[j].get_yticklabels(), fontsize = yTickLabelFontSize)
                        ax[j].set_xlim(min(subx), max(subx)*1.01)
                        if(xTickLabels != []):
                            ax[j].set_xticklabels(xTickLabels)
                            plt.setp(ax[j].get_xticklabels(), fontsize = xTickLabelFontSize)
                        if ylabel != "" and ylabel != []:
                            ax[j].set_ylabel(ylabel[j], fontsize = ylabelFontSize)	
                    ylabel = ""
                else:
		    matplotlib.pyplot.gca().set_color_cycle(['red', 'blue', 'black', 'green', 'yellow'])
                    for i, subx in enumerate(x):
		        if lineType != []:
		            lType = lineType[i]
                        if plot3D:
                            alertMessage("Cannot plot multi 3D figure.")
                        else:
                            ax.plot(subx, y[i], lType ,label=legends[i])
		legendsFromPlot = True

            else:
                lType = ''
		if lineType != []:
		    lType = lineType[0]
                if legends != []:
                    if plot3D:
                        ax.plot(x,y,z,lType,label=legends[0])
                    else:
                        ax.plot(x,y,lType,label=legends[0])
                    legendsFromPlot = True
                else:
                    if plot3D:
                        ax.plot(x,y,z,lType)
                    else:
                        ax.plot(x,y,lType)
	elif graphType == "cdf" or graphType == "ccdf":
		toSetXLim = True
		if xLim != []:
		    toSetXLim = False
		if yLim != []:
                    yLim = [0,1.1]
		if multi:
                        if lineColors == []:
			    #matplotlib.pyplot.gca().set_color_cycle(['red', 'blue', 'green', 'black', 'yellow'])
			    matplotlib.pyplot.gca().set_color_cycle(['blue', 'red', 'green', 'black', 'yellow'])
			    #matplotlib.pyplot.gca().set_color_cycle(['black', 'blue','red', 'green', 'brown', ])
			    #matplotlib.pyplot.gca().set_color_cycle(['blue','black','red', 'green', 'brown', ])
                        else:
                            matplotlib.pyplot.gca().set_color_cycle(lineColors)
		#	matplotlib.pyplot.gca().set_color_cycle(['green', 'black', 'red', 'blue', 'yellow'])
			if len(lineType) != len(x) and lineType != []:
				print "You have to either specify lineType for each plot in case of multi plots or for nothing. If you want solid line for a plot pass \'\' as corresponding argument.", fileName,"IS NOT PLOTTED"
				return
			if toSetXLim:
				xLim = [100000000000000000000000000000000000000000000000000000,0]
			count = 0
			for subX in x:
				cdfx = np.array(subX)
				cdfx.sort()
                                if graphType == "cdf":
                                    cdfy = [float((i+1))/float(len(cdfx)) for i in range(len(cdfx))]
                                elif graphType == "ccdf":
                                    cdfy = [1 - (float((i+1))/float(len(cdfx))) for i in range(len(cdfx))]
                                else:
                                    printErrorMessage("Generating multiyVector. Expected graphtype = cdf or ccdf. got: "+str(graphType))
				#if count == 0:
				if lineType != []:
					lType = lineType[count]
				else:
					lType = ''
				ax.plot(cdfx, cdfy, lType, label=legends[count], linewidth = lineWidth)
				#else:
				#	ax.plot(cdfx, cdfy, label=legends[count],linewidth = 1)
				count+=1
                                if printPlots:
                                    print len(cdfx)
                                    for temp_idx, val in enumerate(cdfx):
                                        print val, ",", cdfy[temp_idx]
				if toSetXLim:
                                    try:
                                        xLim[0] = min(xLim[0],cdfx[0])
					xLim[1] = max(xLim[1],cdfx[-1])
                                    except IndexError:
                                        printErrorMessage("IndexError in multi cdf setLim: " + str(len(cdfx))," PLOTVECTORS.CDF.FORSUBX.IFTOSETXLIM")
                                        print len(cdfx), xLim
			legendsFromPlot = True
			#xLim = [min(min(subX) for subX in x), (max(max(subX) for subX in x))]#*1.2]	# this is not very efficient you are anyways sorting it in for loop and can take from there.
		else:
			cdfx = np.array(x)
                        if mergeBottom:
                            for i, val in enumerate(cdfx):
                                if val <= bottomLimit:
                                    cdfx[i] = bottomMergedValue
                        if mergeTop:
                            for i, val in enumerate(cdfx):
                                if val >= topLimit:
                                    cdfx[i] = topMergedValue
			cdfx.sort()
                        if graphType == "cdf":
                            cdfy = [float((i+1))/float(len(cdfx)) for i in range(len(cdfx))]
                        elif graphType == "ccdf":
                            cdfy = [1 - (float((i+1))/float(len(cdfx))) for i in range(len(cdfx))]
                        else:
                            printErrorMessage("Generating yVector. Expected graphtype = cdf or ccdf. got: "+str(graphType))
                        ax.plot(cdfx, cdfy, linewidth = lineWidth)
			if toSetXLim:
			    xLim = [cdfx[0],cdfx[-1]]
		if toSetXLim:
                    xLim[1] *= 1.05
                    xLim[0] *= 0.95
		if ylabel == "":
                    if graphType == "cdf":
			ylabel = "CDF"
                    elif graphType == "ccdf":
			ylabel = "CCDF"
                    else:
                        printErrorMessage("Setting ylabel. Expected graphtype = cdf or ccdf. got: "+str(graphType))
        elif graphType == "boxplot":
            bp = ax.boxplot(x, 0, '', showfliers = False, showcaps = False, medianprops = {'linestyle':'--', 'linewidth':2})
        if not subPlots:
            if(xLim != []):
                #ax.set_xlim([0.1,xLim[1]])
                ax.set_xlim(xLim)
	    if(yLim != []):
                ax.set_ylim(yLim)
            if logx: #and graphType != "scatter":
                ax.set_xscale('log')
            if logy: #and graphType != "scatter":
                ax.set_yscale('log')
            ax.yaxis.grid(hGrid)
	    ax.xaxis.grid(vGrid)
#	    if logx and xTickLabels==[]: # setting labels for case of log scale with exponential values
#	    	xTickLabels = ax.xaxis.get_majorticklabels()
#	    	print xTickLabels[8]
#	    	for i in range(len(xTickLabels)):
#	    		xTickLabels[i] = str(math.pow(logBase, float(xTickLabels[i])))
#	    if logy and yTickLabels==[]: # setting labels for case of log scale with exponential values
#	    	yTickLabels = ay.yaxis.get_majorticklabels()
#	    	for i in range(len(yTickLabels)):
#	    		yTickLabels[i] = str(math.pow(logBase, float(yTickLabels[i])))

            if xTicks != []:
                ax.set_xticks(xTicks)
            if yTicks != []:
                ax.set_yticks(yTicks)
 
            if(xTickLabels != []):
                if(graphType == "bar"):
                    plt.xticks(x,xTickLabels)
                else:
                    ax.set_xticklabels(xTickLabels)
            if(yTickLabels != []):
                ax.set_yticklabels(yTickLabels)

            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(xTickLabelFontSize)
                #print tick.label.get_text()
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(yTickLabelFontSize)
            for tick in ax.get_xticklabels():
                tick.set_rotation(rotateXTicks)
            for tick in ax.get_yticklabels():
                tick.set_rotation(rotateYTicks)
	    if(xMinorTickLabels != []):
	    	if(xMinorTickLabelsLocator != []):
	    		ax.xaxis.set_minor_locator(ticker.FixedLocator(xMinorTickLabelsLocator))
	    	ax.xaxis.set_minor_formatter(ticker.FixedFormatter(xMinorTickLabels))
	    if(yMinorTickLabels != []):
	    	if(yMinorTickLabelsLocator != []):
	    		ax.yaxis.set_minor_locator(ticker.FixedLocator(yMinorTickLabelsLocator))
	    	ax.yaxis.set_minor_formatter(ticker.FixedFormatter(yMinorTickLabels))
	    for tick in ax.xaxis.get_minor_ticks():
                    tick.label.set_fontsize(18)
	    for tick in ax.yaxis.get_minor_ticks():
                    tick.label.set_fontsize(18)
	    matplotlib.pyplot.gcf().subplots_adjust(bottom=0.17)
            #for ticklabel in ax.get_xticklabels():
            #    print ticklabel.get_text()
            #for ticklabel in ax.get_yticklabels():
            #    print ticklabel.get_text()
	if xlabel != "":
            matplotlib.pyplot.xlabel(xlabel,fontsize = xlabelFontSize)
        if ylabel != "":
            matplotlib.pyplot.ylabel(ylabel, fontsize = ylabelFontSize)	
	if title!="":
	    matplotlib.pyplot.title(title, fontsize = titleFontSize)
	legendsDone = False
        if legendsFromPlot:
            if subPlots:
                legendsDone = True
                handles = []
                labels = []
                for j in range(len(ax)):
                    handle, label = ax[j].get_legend_handles_labels()
                    handles.append(handle[0])
                    labels.append(label[0])
                legend = fig.legend(handles, labels, loc=legendLoc, ncol=len(x), mode = "expand", prop={'size': legendFontSize})
                legend.get_frame().set_linewidth(1)
                legend.get_frame().set_edgecolor('black')
            else:
                handles, labels = ax.get_legend_handles_labels()
		ax.legend(handles[::-1], labels[::-1])
	elif legends != []:
                #legendsDone = True
		patchesSym = []
		patchesText = []
                for i in range(0,len(legends),2):
		    patchesSym.append(mpatches.Circle((0, 0), 1, fc=legends[i],ec="none"))
		    patchesText.append(legends[i+1])
                matplotlib.pyplot.legend(patchesSym,patchesText,prop={'size':legendFontSize},loc=legendLoc)
 
        #ax.legend(loc=legendLoc,prop={'size':legendFontSize})
        if legends != []:
            if not subPlots and not legendsDone:    #   This one is needed when subplots are being plotted.
                pass
                ax.legend(loc=legendLoc,prop={'size':legendFontSize})
 
        save = True

        if returnObject:
            return fig, ax, bp

        if show:
		matplotlib.pyplot.show()
		a = raw_input("Do you want to save this?(y/n): ")
		if a == "y" or a == "Y":
			save = True
		elif a == "n" or a == "N":
			save = False
        if tight:
            temp_bbox_inches = 'tight'
        else:
            temp_bbox_inches = ''
        if save:
		if pdf:
			pp = PdfPages(fileName+'.pdf')
			pp.savefig(fig, bbox_inches = temp_bbox_inches)
			pp.close()
                        infoMessage("Figure saved at: "+fileName+'.pdf')
		if png:
			matplotlib.pyplot.savefig(fileName, bbox_inches = temp_bbox_inches)
                        infoMessage("Figure saved at: "+fileName+'.png')
		if eps:
			matplotlib.pyplot.savefig(fileName+'.eps',format='eps', dpi =1000, bbox_inches = temp_bbox_inches)
                        infoMessage("Figure saved at: "+fileName+'.eps')

def writeToCSV(listOfColumns,columnNames,outputFile,mode='wb'):
	with open(outputFile+'.csv',mode) as fi:
		writer = csv.writer(fi)
		writer.writerow(columnNames)
		for i in range(len(listOfColumns[0])):
		    temp = []
		    for j in range(len(listOfColumns)):
                        if i >= len(listOfColumns[j]):
                            continue
			temp.append(listOfColumns[j][i])
		    writer.writerow(temp)
		#writer.writerows(izip(listOfColumns))

def listToCSVLine(elements):    # converts to string and appends a new line at end
    line_to_return = listToCSVString(elements)+"\n"
    #for idx, ele in enumerate(elements):
    #    line_to_return += str(ele)
    #    if idx != len(elements) - 1:
    #        line_to_return += ","
    return line_to_return

def printErrorMessage(message, source):
    print "ERROR:", message, "at", source

def getFileFirstName(fullName):
	extentionIndex = -4
	for i in range(1,len(fullName)):
		if fullName[-1*i] == '.':
			return fullName[0:-1*i]

def getCoefficientOfVariation(data):
    cv_to_return = float(np.std(data))/float(np.mean(data))
    return cv_to_return

def solveExponentialCDFWithTwoPoints(x1, y1, x2, y2):
    # This is for solving for a and b in y = 1 - e^-(ax +b)
    b = (x2*np.log(1 - y1) - x1*np.log(1 - y2))/(x1 - x2) 
    a = (np.log(1-y1) + b)/(-1*x1) 
    print a,b
    return a,b

def verifyQuadratic(a,b,c,roots): # verifies aX^2 + bX +c roots is a list. Returns the number of roots not satisfied.
    toReturn = 0
    for root in roots:
        rootVal = abs((a*(root**2) + b*root + c))
        if rootVal > 0.001*abs(c):
            alertMessage("Root not valid for equation with a:"+ str(a)+ " b:"+str(b)+" c:"+str(c)+ " rootVal:" +str(rootVal)+" root: "+str(root))
            toReturn +=1 
    return toReturn

def solveQuadratic(a,b,c,returnDiscriminant = False): # solves aX^2 + bX +c returns both roots if solvable else returns NAN for both
    D = b**2 - 4*a*c
    if D < 0:
    #    alertMessage("\nNegative discriminant for equation with a:"+ str(a)+ " b:"+str(b)+" c:"+str(c)+ " D:" +str(D)+"\n")
        if returnDiscriminant:
            return None, None, D
        else:
            return None, None
    else:
        D_root = np.sqrt(D)
    root_1 = (-1*b - D_root)/(2*a)
    root_2 = (-1*b + D_root)/(2*a)
    if returnDiscriminant:
        return root_1, root_2, D
    else:
        return root_1, root_2

def get3SigmaGaussianMeanAndSigmaForTheRange(a, b): #returns mean, sigma
    if a < 0 or b <0:
        alertMessage("get3SigmaGaussianMeanAndSigmaForTheRange says: Akshay didn't design me to work for negative numbers. So, I am returning None, None. It's all his fault.")
        return None, None
    return (a+b)/2, ((abs(b - a))/2)/3 

def printPercentileValuesForTheList(List, identifier = "", percentiles = [1,10,50,90,99], otherStats = False, printPercentileFor = []):
    percentiles.sort()
    percentilesToReturn = []
    if printPercentileFor != []:
        alertMessage("In printPercentileValuesForTheList sorting the input list. This might effect your results.")
        List.sort() #   this needs to be done for printing printPercentileFor doing it here so we might save time in np.percentile as sorting there will be quick.

    print "==========================================================="
    print "Total number of elements:", len(List)
    if otherStats:
        print "Min for "+identifier+": ", float(np.min(List))
    for percentile in percentiles:
        percentileValueOfList = np.percentile(np.array(List), percentile)
        percentilesToReturn.append(percentileValueOfList)
        percentile_str = str(percentile)
        if percentile <10:
            percentile_str += " "
        print "P"+percentile_str+" for "+identifier+": ", float(percentileValueOfList)
    if otherStats:
        print "Max for "+identifier+": ", float(np.max(List))
        print "Sum  of "+identifier+": ", float(sum(List))
        print "Mean for "+identifier+":", float(np.mean(List))
        print "Std for "+identifier+": ", float(np.std(List))
    
    if printPercentileFor != []:
        printPercentileFor.sort()
        List_len = float(len(List))
        for val in printPercentileFor:
            indexToStart = 0
            for idx, val2 in enumerate(List[indexToStart:]):
                if val <= val2:
                    print "Percentile of ", val," for "+identifier+" is: ", float(idx)/List_len
                    indexToStart = idx
                    break
    print "==========================================================="

    return percentilesToReturn
    
def warningMessage(message):
    print colored("WARNING: " + message, "yellow") 

def alertMessage(message):
    sys.stderr.write(colored("ALERT: " + message+"\n", "red"))

def successMessage(message):
    print colored("SUCCESS: " + message, "green")

def infoMessage(message):
    sys.stderr.write(colored("INFO: " + message+"\n", "blue"))
    #print colored("INFO: " + message, "blue")

class ReturnMessage(object):
    def __init__(self, successCode, description, printDescriptionImmediately = False):
        self.successCode = successCode
        self.description = description
        if printDescriptionImmediately:
            print "SuccessCode: ", successCode, "description: ", description

    def success(self):
        return self.successCode

    def describe(self):
        print self.description

def listToCSVString(li):
    string_to_return = ""
    for idx, ele in enumerate(li):
        string_to_return += str(ele)
        if idx != len(li) - 1:
            string_to_return += ","
    return string_to_return

#def jsonToCSVFile(jsonFile, csvFile):
#    import json
#    with open(jsonFile, 'r') as jf:
#        with open(csvFile, 'w') as cf:
#            for line in jf.readlines():
#                temp_j_dict = json.load(line)

def float_to_filename_string(float_input):
    string_input = str(float_input)
    string_to_return = string_input.replace(".", "pt")
    return string_to_return

def roundNToNearestMultipleOfK(n, k):
    #This function is derived from the python code at the following link: https://www.geeksforgeeks.org/round-the-given-number-to-nearest-multiple-of-10/
    # Smaller multiple
    a = int((n//k)*k)
    # Larger multiple
    b = a + k
    # Return of closest of two
    return (b if n - a > b - n else a)
